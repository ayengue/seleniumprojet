package com.yengue.selenium;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.opencsv.CSVReader;

public class WebDriverTest {

	private WebDriver driver;

	@Before()
	public void init() {
//		System.setProperty("webdriver.gecko.driver",
//				"C:\\Users\\abarry\\Desktop\\MAN\\selenium\\src\\ressoures\\geckodriver\\geckodriver.exe");
//		System.setProperty("webdriver.firefox.fin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.ee");
//		driver = new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\abarry\\Desktop\\MAN\\selenium\\src\\ressoures\\chrome\\chromedriver.exe");
//		System.setProperty("webdriver.firefox.fin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.ee");
		driver = new ChromeDriver();
		

		driver.get("http://localhost:4200/signon");
	}

	@Test
	public void testCreationAccountUser() throws IOException {
		CSVReader reader = new CSVReader(
				new FileReader("C:\\Users\\abarry\\Desktop\\MAN\\selenium\\src\\ressoures\\documents\\data.csv"));
		String[] nextLine;
		int iteration = 0;
		while ((nextLine = reader.readNext()) != null) {
			
			if(iteration == 0) {
		        iteration++;  
		        continue;
		    }
			
			System.out.println(nextLine[0] + nextLine[1] + nextLine[2]);
			driver.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys(nextLine[0]);
			driver.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys(nextLine[1]);
			driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys(nextLine[2]);
				
			driver.findElement(By.xpath("/html/body/app-root/app-signon/mat-card/mat-card-content/form/mat-card-actions/button")).click();
			driver.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("");
			driver.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("");
			driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("");

		}
		
	}

}
